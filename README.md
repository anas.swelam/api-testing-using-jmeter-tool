# API Testing using Jmeter tool

**Steps**

1. Install Jmeter "apache-jmeter-5.3" or any version.
2. Using Blazemeter to record our scenario.
3. Import the config. file with (.jmx) extenssion to Jmeter.
4. Open the file on Jmeter.
5. Edit the number of concurrent users on "Thread Group"
6. run the code.
7. View the result (Summary report)
8. Export the summary report to your machine with (.CSV) file



